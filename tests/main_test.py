"""Unit tests"""
from app.main import inc


def test_inc():
    """Test inc() function"""
    assert inc(0) == 1
    assert inc(10000000) == 10000001
    assert inc(-5555) == -5554
