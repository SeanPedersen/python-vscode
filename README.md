# VS Code Python Project Boilerplate
![build status](https://gitlab.com/SeanPedersen/python-vscode/badges/master/pipeline.svg)
## Features
- MyPy: Type annotation checking on save
- Black: Code formatting on save
- PyLint: Static code analysis on save
- Bandit: Code security analysis on save
- PyTest: Testing framework
- Gitlab CI: On push run PyLint, Bandit, MyPy & PyTest if .py file/s changed

## Setup
- Setup virtual or conda environment
- Install dev tooling: ```$ pip install -r requirements.dev.txt```
- Open up VS Code: ```$ code .```
- (CTRL+SHIFT+P) then type & select: ```Python: Select Interpreter```

## Usage
- Run all tests & show coverage stats: ```$ pytest -v --cov```
- Run PyLint: ```$ pylint **/*.py```
- Run MyPy: ```$ mypy **/*.py```
- Run Bandit: ```$ bandit --exclude tests/ -r .```
- Run app: ```$ python -m app.main```
