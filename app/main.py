"""Awesome Python"""


def inc(value: int) -> int:
    """Increment value by 1"""
    value += 1
    return value


if __name__ == "__main__":
    print(inc(0))
